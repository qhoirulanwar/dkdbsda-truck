### dkdbsda-truck

Download or Clone dkdbsda-truck Project from Bitbucket
```sh
$ git clone https://qhoirulanwar@bitbucket.org/qhoirulanwar/dkdbsda-truck.git
$ cd dkdbsda-truck
$ npm install
```

### Get your local Git repository on Bitbucket
Step 1: Switch to your repository's directory
```sh
$ cd /path/to/your/repo
```

Step 2: Connect your existing repository to Bitbucket
```sh
$ git remote add origin https://qhoirulanwar@bitbucket.org/qhoirulanwar/dkdbsda-truck.git
$ git push -u origin master
```