const cryptoRandomString = require('crypto-random-string');
const pool = require('../utils/dbconnection')

module.exports = {
    allQuarry: async (request, reply) => {
        // return ({ hello: 'world' })

        try {
            const query = `SELECT * FROM tquarry`

            await pool.query(query)
            .then(rows => {
                // reply.send(rows)
                reply.send(rows)
            })
            
        } catch (error) {
            throw error
        }
    },

    postQuarry: async (request, reply) => {
        const id = cryptoRandomString({length: 6})
        const quarryNama = request.body.quarryNama

        try {
            const query = `INSERT INTO tquarry (quarry_id, quarry_nama) VALUES ('${id}', '${quarryNama}')`

            await pool.query(query)
            .then(rows => {
                // reply.send(rows)
                reply.send({
                    status: true,
                    message : `Quarry ${quarryNama} Berhasil ditambah`
                })
            })
            
        } catch (error) {
            throw error
        }
    }
}