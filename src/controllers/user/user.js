const cryptoRandomString = require('crypto-random-string');
const pool = require('../../utils/dbconnection')

module.exports = {
    fastlogin: async (request, reply) => {
        // return ({ hello: 'world' })
        const user = request.body.user

        try {
            const query = `SELECT * FROM tuser WHERE user_id = '${user}'`

            await pool.query(query)
            .then(rows => {

                if (rows.length == 1) {
                    if (rows[0].user_id == user) {
                        reply.send({
                            status: true,
                            message: 'Login Berhasil'
                        })
                    }
                } else {
                    reply.send({
                        status: false,
                        message: 'Password Salah'
                    })
                }
                
            })
            
        } catch (error) {
            throw error
        }
    },

    allUser: async (request, reply) => {

        try {
            const query = `SELECT * FROM tuser`

            await pool.query(query)
            .then(rows => {
                reply.send(rows)
            })
            
        } catch (error) {
            throw error
        }
    },

    postUser: async (request, reply) => {
        const id = cryptoRandomString({length: 6})
        const userNama = request.body.userNama

        try {
            const query = `INSERT INTO tuser (user_id, user_nama) VALUES ('${id}', '${userNama}')`

            await pool.query(query)
            .then(rows => {
                // reply.send(rows)
                reply.send({
                    status: true,
                    message : `User ${userNama} Berhasil ditambah`,
                    data: {
                        user: id
                    }
                })
            })
            
        } catch (error) {
            throw error
        }
    }

}