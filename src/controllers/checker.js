const cryptoRandomString = require('crypto-random-string');
const pool = require('../utils/dbconnection')

module.exports = {
    trx: async (request, reply) => {
        const qrcode = request.body.qrcode
        const user = request.body.user

        const truck = await getTruck(qrcode)
        const suplay = await getSuplay(truck.suplay_id)
        const quarry = await getQuarry(truck.quarry_id)

        try {

            await trxChecker(truck, suplay, quarry)
            .then(()=>{
                reply.send({
                    status: true,
                    message : 'Checker Log Berhasil',
                    data : {
                        truck: truck,
                        suplay: suplay,
                        quarry: quarry
                    }
                })
            }).catch((err)=>{
                reply.send(err)
            })

        } catch (error) {
            throw error
        }

        function trxChecker(truck, suplay, quarry) {
            const checkerId = new Date().getTime()
            const logId = cryptoRandomString({length: 16})

            const queryChecker = `INSERT INTO tchecklog (checklog_id, createdAt, user_id, suplay_id, quarry_id) VALUES (?, CONVERT_TZ(NOW(), @@session.time_zone, '+07:00'), ?, ?, ?)`
            const queryCheckerTruck = `INSERT INTO tchecklog_truck (id, checklog_id, truck_id, suplay_nama, quarry_nama, truck_nopol, truck_driver, truck_index, truck_bak_p, truck_bak_l, truck_bak_t) 
                                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`

            return new Promise((resolve, reject) => {
                pool.getConnection()
                .then(conn => {
                    conn.beginTransaction()
                    .then(() => {
                        const trxData = [checkerId, user, suplay.suplay_id, quarry.quarry_id]
                        return conn.query(queryChecker, trxData)
                    })
                    .then(() => {
                        const checkerData = [logId, checkerId, truck.truck_id, suplay.suplay_nama, quarry.quarry_nama, truck.truck_nopol, truck.truck_driver, truck.truck_index, truck.truck_bak_p, truck.truck_bak_l, truck.truck_bak_t]
                        return conn.query(queryCheckerTruck, checkerData)
                    })
                    .catch((err) => {
                        conn.rollback()
                        reject(err)
                    })
                    .then(() => {
                        conn.commit()
                        resolve(true)
                    })
                    .then((res) => {
                        conn.end();
                    })
                })
            })

        };

        function getQuarry(quarryId) {
            return new Promise((resolve, reject) => {
                const query = `SELECT * FROM tquarry WHERE quarry_id = '${quarryId}'`
                pool.query(query)
                .then(rows => {
                    resolve(rows[0])
                })
                .catch(err => {
                    reject(err)
                })
            })
        };

        function getSuplay(suplayId) {
            return new Promise((resolve, reject) => {
                const query = `SELECT * FROM tsuplayer WHERE suplay_id = '${suplayId}'`
                pool.query(query)
                .then(rows => {
                    resolve(rows[0])
                })
                .catch(err => {
                    reject(err)
                })
            })
        };

        function getTruck(qrcode) {
            return new Promise((resolve, reject) => {
                const query = `SELECT * FROM ttruck WHERE truck_id = '${qrcode}'`
                pool.query(query)
                .then(rows => {
                    resolve(rows[0])
                })
                .catch(err => {
                    reject(err)
                })
            })
        };

    }
}