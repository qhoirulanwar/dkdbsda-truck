const cryptoRandomString = require('crypto-random-string');
const pool = require('../utils/dbconnection')

module.exports = {
    allSuplayer: async (request, reply) => {
        // return ({ hello: 'world' })
        try {
            const query = `SELECT * FROM tsuplayer`

            await pool.query(query)
            .then(rows => {
                // reply.send(rows)
                reply.send(rows)
            })
            
        } catch (error) {
            throw error
        }
    },

    postSuplayer: async (request, reply) => {
        const id = cryptoRandomString({length: 6})
        const  quarryId = request.body.quarryId
        const suplayerNama = request.body.suplayerNama

        try {
            const query = `INSERT INTO tsuplayer (suplay_id, suplay_nama) VALUES ('${id}', '${suplayerNama}')`

            await pool.query(query)
            .then(rows => {
                // reply.send(rows)
                reply.send({
                    status: true,
                    message : `Suplayer ${suplayerNama} Berhasil ditambah`
                })
            })
            
        } catch (error) {
            throw error
        }
    }
}