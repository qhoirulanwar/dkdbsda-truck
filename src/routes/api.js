const fs = require('fs');
const carbone = require('carbone');
module.exports = (fastify, opts, next) => {

    // api
    fastify.get('/', require('../controllers/index').index)

    // Quarry
    fastify.get('/quarry', require('../controllers/quarry').allQuarry)
    fastify.post('/quarry', require('../controllers/quarry').postQuarry)

    // Suplayer
    fastify.get('/suplayer', require('../controllers/suplayer').allSuplayer)
    fastify.post('/suplayer', require('../controllers/suplayer').postSuplayer)

    // Truck
    fastify.get('/truck', require('../controllers/truck').allTruck)
    fastify.get('/truck/generateqr', require('../controllers/truck').generateTruckID)
    fastify.post('/truck', require('../controllers/truck').postTruck)
    fastify.patch('/truck', require('../controllers/truck').regTruck)

    // User
    fastify.get('/user', require('../controllers/user/user').allUser)
    fastify.post('/user', require('../controllers/user/user').postUser)
    fastify.post('/user/auth', require('../controllers/user/user').fastlogin)


    // Checker
    fastify.post('/checker', require('../controllers/checker').trx)

    //laporan
    fastify.get('/report/:tglstart/:tglend', require('../controllers/laporan').daily)
    fastify.get('/report/:idSup/:tglstart/:tglend', require('../controllers/laporan').dailyPerSuplay)
    fastify.get('/report/now-count', require('../controllers/laporan').nowcount)
    fastify.get('/report/xls/:tglstart/:tglend', require('../controllers/laporan').xlsExport)

    fastify.get('/stream', async function (request, reply) {
        // Your code
        // const file = __dirname + '/flat_table.ods';
        const file = './src/templates/flat_table.ods';
        const filestream = await fs.createReadStream(file);
        // filestream.pipe();

        await reply
            .code(200)
            .header('Content-disposition', 'attachment; filename=file.xls')
            .header('Content-Type', 'application/vnd.ms-excel')
            .send(filestream)

        // try {
        //     fs.unlinkSync(file);
        //     console.log('successfully deleted /tmp/hello');
        // } catch (err) {
        //     // handle the error
        //     console.log(err);
        // }
    })

    fastify.get('/streams', async function (request, reply) {

        const temporaryFile = './src/templates/temp/report_trx.ods'

        const data = [
            {
                movieName: 'Matrix',
                actors: [{
                    firstname: 'Keanu',
                    lastname: 'Reeves'
                }, {
                    firstname: 'Laurence',
                    lastname: 'Fishburne'
                }, {
                    firstname: 'Carrie-Anne',
                    lastname: 'Moss'
                }]
            },
            {
                movieName: 'Back To The Future',
                actors: [{
                    firstname: 'Michael',
                    lastname: 'J. Fox'
                }, {
                    firstname: 'Christopher',
                    lastname: 'Lloyd'
                }]
            }
        ];

        await generateFile(data).then(() => {
            const filestream = fs.createReadStream(temporaryFile)

            reply
                .code(200)
                .header('Content-disposition', 'attachment; filename=file.xls')
                .header('Content-Type', 'application/vnd.ms-excel')
                .send(filestream)
        }).then(() => {

            fs.unlinkSync(temporaryFile);
            console.log('successfully deleted /tmp/hello');
        })

        function generateFile(data) {
            return new Promise((resolve, reject) => {
                carbone.render('./src/templates/flat_table.ods', data, function (err, result) {
                    // if (err) return console.log(err);
                    if (err) {
                        console.log(err)
                        reject(err)
                    } else {
                        fs.writeFile('./src/templates/temp/report_trx.ods', result, (err) => {
                            if (err) reject(err);
                            console.log('The file has been saved!');
                            resolve(true)
                        });
                    }
                })
            })
        }
    })

    next()
}