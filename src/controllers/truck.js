const cryptoRandomString = require('crypto-random-string');
const pool = require('../utils/dbconnection')

module.exports = {
    allTruck: async (request, reply) => {
        // return ({ hello: 'world' })

        try {
            const query = `SELECT * FROM ttruck`

            await pool.query(query)
                .then(rows => {
                    // reply.send(rows)
                    reply.send(rows)
                })

        } catch (error) {
            throw error
        }
    },

    generateTruckID: async (request, reply) => {
        const id = cryptoRandomString({ length: 24 })

        try {
            const query = `INSERT INTO ttruck (truck_id) VALUES ('${id}')`
            const count = `SELECT COUNT(*) AS total FROM ttruck`

            await pool.query(query)
                .then(rows => {
                    console.log(rows);

                })
            await pool.query(count)
                .then(rows => {
                    reply.send({
                        qrcode: id,
                        total: rows[0].total
                    })
                })

        } catch (error) {
            throw error
        }
    },

    postTruck: async (request, reply) => {
        const id = cryptoRandomString({ length: 24 })
        const suplayId = request.body.suplayerId
        const platnopol = request.body.platnopol
        const driver = request.body.driver
        const index = request.body.index
        const bak = request.body.bak

        try {
            const query = `INSERT INTO ttruck 
                            (truck_id, suplay_id, truck_nopol, truck_driver, truck_index, truck_bak_p, truck_bak_l, truck_bak_t) 
                            VALUES ('${id}', '${suplayId}', '${platnopol}', '${driver}', '${index}', '${bak.p}', '${bak.l}', '${bak.t}')`

            await pool.query(query)
                .then(rows => {
                    // reply.send(rows)
                    reply.send({
                        status: true,
                        message: `Truk Berhasil ditambah`
                    })
                })

        } catch (error) {
            throw error
        }
    },

    regTruck: async (request, reply) => {
        const id = request.body.truck_id
        const suplayId = request.body.suplay_id
        const quarryId = request.body.quarry_id
        const platnopol = request.body.truck_nopol
        const driver = request.body.truck_driver
        const index = request.body.truck_index
        const bak_p = request.body.truck_bak_p
        const bak_l = request.body.truck_bak_l
        const bak_t = request.body.truck_bak_t

        try {
            const queryPost = `INSERT INTO ttruck 
                            (truck_id, suplay_id, quarry_id, truck_nopol, truck_driver, truck_index, truck_bak_p, truck_bak_l, truck_bak_t) 
                            VALUES ('${id}', '${suplayId}', '${quarryId}', '${platnopol}', '${driver}', '${index}', '${bak_p}', '${bak_l}', '${bak_t}')`

            const queryUpdate = `UPDATE ttruck 
                            SET suplay_id='${suplayId}', quarry_id='${quarryId}', truck_nopol='${platnopol}', truck_driver='${driver}', truck_index='${index}', truck_bak_p='${bak_p}', truck_bak_l='${bak_l}', truck_bak_t='${bak_t}'
                            where truck_id='${id}'`

            await pool.query(queryUpdate)
                .then(rows => {
                    // reply.send(rows)
                    reply.send({
                        status: true,
                        message: `Truk ${platnopol} Berhasil Terdaftar`
                    })
                })

        } catch (error) {
            throw error
        }
    },

    truckDetil: async (request, reply) => {
        // return ({ hello: 'world' })

        try {
            const query = `SELECT * FROM tquarry`

            await pool.query(query)
                .then(rows => {
                    // reply.send(rows)
                    reply.send(rows)
                })

        } catch (error) {
            throw error
        }
    }

}