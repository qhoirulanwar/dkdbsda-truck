const fs = require('fs');
const carbone = require('carbone');
const cryptoRandomString = require('crypto-random-string');
const pool = require('../utils/dbconnection')

module.exports = {
    nowcount: async (request, reply) => {
        const time = new Date(), date = time.getDate(), year = time.getFullYear(), month = time.getMonth(); // January is 0 not 1
        const dateString = year + "-" + (month + 1) + "-" + date;
        console.log(dateString);


        const query = `SELECT COUNT(*) AS total FROM tchecklog INNER JOIN tchecklog_truck ON tchecklog.checklog_id=tchecklog_truck.checklog_id
                        WHERE createdAt >= '${dateString} 00:00:00' AND createdAt <= '${dateString} 23:59:59' `

        await pool.query(query)
            .then(rows => {
                // console.log(rows);
                reply.send(rows[0])
            })

        // reply.send(date)
    },

    daily: async (request, reply) => {
        console.log(request.params);

        // const idSuplayer = request.params.idSup
        const date = {
            start: request.params.tglstart,
            end: request.params.tglend
        }

        const query = `SELECT *, DATE_FORMAT(createdAt, "%d-%m-%Y") AS tanggal, convert(createdAt, TIME) as waktu 
                FROM tchecklog INNER JOIN tchecklog_truck ON tchecklog.checklog_id=tchecklog_truck.checklog_id
                WHERE createdAt >= '${date.start} 00:00:00' AND createdAt <= '${date.end} 23:59:59'
                ORDER BY createdAt ASC`

        await pool.query(query)
            .then(rows => {
                // console.log(rows);
                reply.send(rows)
            })
    },

    dailyPerSuplay: async (request, reply) => {
        console.log(request.params);

        const idSuplayer = request.params.idSup
        const date = {
            start: request.params.tglstart,
            end: request.params.tglend
        }

        const query = `SELECT *, DATE_FORMAT(createdAt, "%d-%m-%Y") AS tanggal, convert(createdAt, TIME) as waktu 
                FROM tchecklog INNER JOIN tchecklog_truck ON tchecklog.checklog_id=tchecklog_truck.checklog_id
                WHERE createdAt >= '${date.start} 00:00:00' AND createdAt <= '${date.end} 23:59:59' AND tchecklog.suplay_id = '${idSuplayer}'
                ORDER BY createdAt ASC`

        await pool.query(query)
            .then(rows => {
                // console.log(rows);
                reply.send(rows)
            })
    },

    xlsExport: async (request, reply) => {
        console.log(request.params);

        // const idSuplayer = request.params.idSup
        const date = {
            start: request.params.tglstart,
            end: request.params.tglend
        }

        // let data

        const query = `SELECT *, DATE_FORMAT(createdAt, "%d-%m-%Y") AS tanggal, convert(createdAt, TIME) as waktu 
                FROM tchecklog INNER JOIN tchecklog_truck ON tchecklog.checklog_id=tchecklog_truck.checklog_id
                WHERE createdAt >= '${date.start} 00:00:00' AND createdAt <= '${date.end} 23:59:59'
                ORDER BY createdAt ASC`

        const data = await getData(query)
        const temporaryFile = './src/templates/temp/report_trx.ods'

        await generateFile(data).then(() => {
            const filestream = fs.createReadStream(temporaryFile)

            reply
                .code(200)
                .header('Content-disposition', 'attachment; filename=file.xls')
                .header('Content-Type', 'application/vnd.ms-excel')
                .send(filestream)
        }).then(() => {

            fs.unlinkSync(temporaryFile);
            console.log('successfully deleted /tmp/hello');
        })

        // await pool.query(query)
        //     .then(rows => {
        //         // console.log(rows);
        //         // reply.send(rows)

        //         data = rows
        //     })
        //     .then(rows => {

        //         console.log(data);
        //     })

        function getData(query) {
            return new Promise((resolve, reject) => {
                pool.query(query)
                    .then(rows => {
                        resolve(rows)
                        // console.log(data);
                    })
                    .catch((err) => {
                        reject(err)
                    })
            })
        }

        function generateFile(data) {
            return new Promise((resolve, reject) => {
                carbone.render('./src/templates/laporan_transaksi.xlsx', data, function (err, result) {
                    // if (err) return console.log(err);
                    if (err) {
                        console.log(err)
                        reject(err)
                    } else {
                        fs.writeFile('./src/templates/temp/report_trx.ods', result, (err) => {
                            if (err) reject(err);
                            console.log('The file has been saved!');
                            resolve(true)
                        });
                    }
                })
            })
        }
    }
}